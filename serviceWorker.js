// Def de la raiz
let rootString = '/';
const stage = 'dev'; // 'dev' o 'prod'
if (stage === 'prod') {
    rootString = '/pwa-base/';
}

// Definir los caches a utilizar
const CACHE_APP_SHELL = 'mi-app-shellv1';
const CACHE_DINAMICO = 'cache-dinamicov1';
const CACHE_INMUTABLE = 'cache-inmutablev1';

self.addEventListener('install', event => {
    console.log('Se instala sw... Nuevo.');

    // Creación del cache
    const cacheAppShell = caches.open(CACHE_APP_SHELL).then(cache => {
        return cache.addAll([
            rootString,
            rootString + '/',
            rootString + '/index.html',
            rootString + '/css/style.css',
            rootString + '/js/app.js',
            rootString + '//img/bannerBolsaDeTrabajo.jpeg',
            rootString + '/img/UTH-LOGO-VERT.png',
            rootString + '/img/icons/favicon.ico',
            rootString +'/manifest.json'
        ]);
    });

    // Guardar los elementos INMUTABLES
    const cacheInmutable = cache.open(CACHE_INMUTABLE).then(cache => {
        return cache.addAll([
            'https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous',
            'https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css',
            'https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous',
        ]);
    });
    event.waintUntill(cacheAppShell);
});

// Función para borrar cache recursivamente en promesa:
function borrarCache() {
    Caches.open(CACHE_DINAMICO).then(
        cache => {
            cache.keys().then(keys => {
                console.log("Número de elementos en cache dinamico: " +keys.length);
                // Limitar a 3 elementos
                if (keys.length > 3) {
                    cache.delete(keys[keys.length -1]).then(
                        borrarCache()
                    );
                }
            });
        }
    )
}

self.addEventListener('activate', event => {
    console.log('El sw se ha activado.');
    event.waintUntill(borrarCache());
});

self.addEventListener('fetch', event => {
    console.log(event.request.url);

    // Estrategia de gestión del cache - Cache only
    /*event.respondWith(
        caches.match(event.request)
    );*/

    // Estrategia del cache - Cahe y después red
    event.respondWith(caches.match(event.request)
    .then(res => {
        // Si la resp existe en el cache, regresar:
        if (res) return res;
        else {
            console.log('No se encontró en el cache el ', event.request.url);
            // Si la resp no existe, traerla del sitio o el origen:
            fetch(event.request).then(nuevoElemento => {
                caches.open(CACHE_DINAMICO).then(cache => {
                    cache.put(event.request, nuevoElemento);
                    borrarCache();
                });
                return nuevoElemento.clone();
            }).
            catch(error => {
                console.log('Error en fetch, ', error);
            })
        }
    }));
});