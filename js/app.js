// Validar si se puede tener sw
if(navigator.serviceWorker)
{
    console.log("SI esta disponible el trabajo con el Service Worker.");
    
    // Instalar el sw
    navigator.serviceWorker.register('/pwa-base/sw.js');
    console.log("NO esta disponible el trabajo con el Service Worker en este Browser.")
}
else
{
    console.log("NO esta disponible el trabajo con el Service Worker en este Browser.");
}